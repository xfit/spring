package coach.xfitness.xfit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XfitApplication {

	public static void main(String[] args) {
		SpringApplication.run(XfitApplication.class, args);
	}

}
